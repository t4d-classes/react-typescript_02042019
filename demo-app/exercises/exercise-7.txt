Exercise 7

1. Utilize ToolHeader in the Car Tool component (replacing the current header).

2. Move Car Table to its own component, passing in "cars" as a prop to the new Car Table component. Replace the table in Car Tool with the Car Table component. IMPORTANT! Do not move car form. Leave car form where it is.

3. Ensure it works.