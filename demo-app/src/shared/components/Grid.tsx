import React from 'react';

export interface GridLayoutProps extends React.HTMLAttributes<HTMLDivElement> {
  gridClass: string;
}

export const GridLayout = ({
  gridClass, children, className, ...props
}: GridLayoutProps) =>
  <div className={`${className} ${gridClass}`} {...props}>
    {children}
  </div>;

export interface GridAreaProps extends React.HTMLAttributes<HTMLDivElement> {
  gridArea: string;
}


export const GridArea = ({ gridArea, children, ...props }: GridAreaProps) =>
  <div style={{ gridArea }} {...props}>
    {children}
  </div>;

