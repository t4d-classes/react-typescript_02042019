import React from 'react';

import './ToolHeader.css';

export const ToolHeader = ({ headerText }: { headerText: string }) => {

  return <header className="ToolHeader">
    <h2>{headerText}</h2>
  </header>;
};