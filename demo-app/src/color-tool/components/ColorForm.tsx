import React, { useState, useRef, useEffect } from 'react';

import { Color } from '../models/Color';

interface ColorFormProps {
  onSubmitColor: (color: Color) => void;
}

export const ColorForm = ({ onSubmitColor }: ColorFormProps) => {

  const nameInputRef = useRef(null) as React.RefObject<HTMLInputElement>;



  useEffect(() => {
    if (nameInputRef.current) {
      nameInputRef.current.focus();
    }

    // const subscriber = someServer.subscribe();
    console.log('component mounted');

    return () => {
      // this function will run before the use effect runs again and when the component is removed
      // subscriber.unsubscribe();
      console.log('component unmounted');
    }
  }, []);

  const [ colorForm, setColorForm ] = useState({
    name: '',
    hexCode: '',
  });

  const submitColor = () => {
    onSubmitColor(colorForm);
    setColorForm({
      name: '',
      hexCode: '',
    });
  }

  return <form>
    <div>
      <label htmlFor="name-input">Color Name:</label>
      <input type="text" id="name-input" name="name"
        value={colorForm.name} ref={nameInputRef}
        onChange={e => setColorForm({ ...colorForm, name: e.target.value })} />
    </div>
    <div>
      <label htmlFor="hexcode-input">Color HexCode:</label>
      <input type="text" id="hexcode-input" name="hexcode"
        value={colorForm.hexCode}
        onChange={e => setColorForm({ ...colorForm, hexCode: e.target.value })} />
    </div>
    <button type="button" onClick={submitColor}>Add Color</button>
  </form>;

};