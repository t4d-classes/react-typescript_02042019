import React, { useState } from 'react';

import { Color } from '../models/Color';

import { ToolHeader } from '../../shared/components/ToolHeader';
import { ColorList } from './ColorList';
import { ColorForm } from './ColorForm';

interface ColorToolProps {
  colors: Color[];
}

export const ColorTool = (props: ColorToolProps) => {

  const [ colors, setColors ] = useState(props.colors.concat());

  const addColor = (color: Color) => {
    setColors(colors.concat({
      ...color,
      id: Math.max(...colors.map(c => c.id) as number[], 0) + 1,
    }));
  };

  const deleteColor = (colorId: number | undefined) => {
    setColors(colors.filter(color => color.id !== colorId));
  };

  return <>
    <ToolHeader headerText="Color Tool" />
    <ColorList colors={colors} onDeleteColor={deleteColor} />
    <ColorForm onSubmitColor={addColor} />
  </>;
};

export default ColorTool;
