import React from 'react';

import { Car } from '../models/Car';

import { ToolHeader } from '../../shared/components/ToolHeader';
import { CarTable } from './CarTable';
import { CarForm } from './CarForm';

export interface CarToolProps {
  cars: Car[];
}

interface CarToolState {
  cars: Car[];
  editCarId: number;
}

export class CarTool extends React.Component<CarToolProps, CarToolState> {

  state = {
    cars: this.props.cars.concat(),
    editCarId: -1,
  };

  editCar = (carId: number) => {
    this.setState({
      editCarId: carId,
    });
  };

  cancelCar = () => {
    this.setState({
      editCarId: -1,
    });    
  };

  addCar = (car: Car) => {

    const newCar = {
      ...car,
      id: Math.max(...this.state.cars.map(c => c.id) as number[], 0) + 1,
    };

    this.setState({
      cars: this.state.cars.concat(newCar),
      editCarId: -1,
    });
  };

  deleteCar = (carId: number) => {
    this.setState({
      cars: this.state.cars.filter(c => c.id !== carId),
      editCarId: -1,
    });
  };

  saveCar = (car: Car) => {

    const carsCopy = this.state.cars.concat();

    const carIndex = carsCopy.findIndex(c => c.id === car.id);
    carsCopy[carIndex] = car;

    this.setState({
      cars: carsCopy,
      editCarId: -1,
    });
  };

  render() {
    return <>
      <ToolHeader headerText="Car Tool" />
      <CarTable cars={this.state.cars} editCarId={this.state.editCarId}
        onEditCar={this.editCar} onDeleteCar={this.deleteCar}
        onSaveCar={this.saveCar} onCancelCar={this.cancelCar} />
      <CarForm onSubmitCar={this.addCar} buttonText="Add Car" />
    </>;
  }
};

export default CarTool;
