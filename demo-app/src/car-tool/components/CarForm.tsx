import React from 'react';

import { Car } from '../models/Car';

import './CarForm.css';

export interface CarFormProps {
  onSubmitCar: (car: Car) => void;
  buttonText: string;
}

interface CarFormState {
  make: string;
  model: string;
  year: number;
  color: string;
  price: number | string;
  [ x: string ]: any;
}

export class CarForm extends React.Component<CarFormProps, CarFormState> {

  initialFormState = {
    make: '',
    model: '',
    year: 1900,
    color: '',
    price: 0,
  };

  state = {
    ...this.initialFormState
  };

  makeInput: React.RefObject<HTMLInputElement>;

  constructor(props: CarFormProps) {
    super(props);

    this.makeInput = React.createRef();

  }

  componentDidMount() {
    if (this.makeInput.current) {
      this.makeInput.current.focus();
    }
  }

  change = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [ e.target.name ]: e.target.type === 'number'
        ? Number(e.target.value) : e.target.value,
    }, () => {
      console.log(this.state);
    });
  };

  submitCar = () => {
    this.props.onSubmitCar(this.state);

    this.setState({
      ...this.initialFormState,
    });
  };

  render() {
    return <form className="CarForm">
      <div>
        <label htmlFor="make-input">Make:</label>
        <input type="text" id="make-input" name="make" ref={this.makeInput}
          value={this.state.make} onChange={this.change} />
      </div>
      <div>
        <label htmlFor="model-input">Model:</label>
        <input type="text" id="model-input" name="model"
          value={this.state.model} onChange={this.change} />
      </div>
      <div>
        <label htmlFor="year-input">Year:</label>
        <input type="number" id="year-input" name="year"
          value={this.state.year} onChange={this.change} />
      </div>
      <div>
        <label htmlFor="color-input">Color:</label>
        <input type="text" id="color-input" name="color"
          value={this.state.color} onChange={this.change} />
      </div>
      <div>
        <label htmlFor="price-input">Price:</label>
        <input type="number" id="price-input" name="price"
          value={this.state.price} onChange={this.change} />
      </div>
      <button type="button" onClick={this.submitCar}>{this.props.buttonText}</button>
    </form>;
  }


}