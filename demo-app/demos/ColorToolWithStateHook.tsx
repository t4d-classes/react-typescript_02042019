import React, { useState } from 'react';

import { Color } from '../models/Color';

interface ColorToolProps {
  colors: Color[];
}

export const ColorTool = (props: ColorToolProps) => {

  const [ colorForm, setColorForm ] = useState({
    name: '',
    hexCode: '',
  });
  const [ colors, setColors ] = useState(props.colors.concat());

  const addColor = () => {
    setColors(colors.concat({
      ...colorForm,
      id: Math.max(...colors.map(c => c.id) as number[], 0) + 1,
    }));
    setColorForm({ name: '', hexCode: '' });
  };

  return <>
    <header>
      <h1>Color Tool</h1>
    </header>
    <ul>
      {colors.map(color => <li key={color.id}>{color.name} - {color.hexCode}</li>)}
    </ul>
    <form>
      <div>
        <label htmlFor="name-input">Color Name:</label>
        <input type="text" id="name-input" name="name"
          value={colorForm.name}
          onChange={e => setColorForm({ ...colorForm, name: e.target.value })} />
      </div>
      <div>
        <label htmlFor="hexcode-input">Color HexCode:</label>
        <input type="text" id="hexcode-input" name="hexcode"
          value={colorForm.hexCode}
          onChange={e => setColorForm({ ...colorForm, hexCode: e.target.value })} />
      </div>
      <button type="button" onClick={addColor}>Add Color</button>
    </form>
  </>;
};