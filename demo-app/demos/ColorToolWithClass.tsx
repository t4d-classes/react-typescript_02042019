import React from 'react';

interface ColorToolProps {
  colors: string[];
}

interface ColorToolState {
  color: string;
  colors: string[];
  [ x: string ]: any;
}

export class ColorTool extends React.Component<ColorToolProps, ColorToolState> {

  state = {
    color: '',
    colors: this.props.colors.concat(),
  };

  change = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [ e.target.name ]: e.target.type === 'number'
        ? Number(e.target.value) : e.target.value,
    }, () => {
      console.log(this.state);
    });
  };

  addColor = () => {

    this.setState({
      colors: this.state.colors.concat(this.state.color),
      color: '',
    });

  };

  // constructor(props: ColorToolProps) {
  //   super(props);

  //   this.state = {
  //     color: '',
  //   };

  //   this.change = this.change.bind(this);
  // }

  // change(e: React.ChangeEvent<HTMLInputElement>) {
  //   this.setState({
  //     color: e.target.value,
  //   });
  // }

  render() {
    return <>
      <header>
        <h1>Color Tool</h1>
      </header>
      <ul>
        {this.state.colors.map(color => <li key={color}>{color}</li>)}
      </ul>
      <form>
        <div>
          <label htmlFor="color-input">New Color:</label>
          <input type="text" id="color-input" name="color" value={this.state.color} onChange={this.change} />
        </div>
        <button type="button" onClick={this.addColor}>Add Color</button>
      </form>
    </>;
  }

};